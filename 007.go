package main

import (
	"fmt"
)

func main() {
	x := 1

	for x <= 5 {
		fmt.Println(x)
		x++
	}

	for x := 10; x <= 20; x += 2 {
		fmt.Println(x)
		if x >= 18 {
			break
		}
	}

	y := 10 / 2

	switch y {
	case 2:
		fmt.Println("n")
	case 5:
		fmt.Println("y")
	default:
		fmt.Println("o")
	}
}
