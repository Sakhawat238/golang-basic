package main

import (
	"fmt"
	"math"
)

func main() {
	var num1 int = 10
	var num2 int = 8
	add := num1 + num2
	sub := num1 - num2
	mul := num1 * num2
	div := num1 / num2
	mod := num1 % num2
	sqr := math.Sqrt(25)
	fmt.Println(add, sub, mul, div, mod, sqr)

	var num3 float64 = 10.50
	var num4 float64 = 8.25
	add1 := num3 + num4
	sub1 := num3 - num4
	mul1 := num3 * num4
	div1 := num3 / num4
	fmt.Println(add1, sub1, mul1, div1)

}
