package main

import (
	"fmt"
)

func main() {
	var a [5]int
	fmt.Println(a)

	for i := 0; i < 5; i++ {
		a[i] = i
	}
	fmt.Println(a)

	b := [3]string{"abc", "def", "ghi"}
	fmt.Println(b)

	c := [2][4]int{{1, 2, 3, 4}, {5, 6, 7, 8}}
	sum := 0
	for i := 0; i < len(c); i++ {
		for j := 0; j < len(c[i]); j++ {
			sum += c[i][j]
		}
	}
	fmt.Println(sum)
}
