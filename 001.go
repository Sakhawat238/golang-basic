package main

import "fmt"

func main() {
	var number1 int = 1000
	var number2 = 200
	var number3 = 50.05
	number4 := 100
	var number5 int
	var name string = "Slyrak"
	fmt.Println(number1, number2, number3, number4, number5, name)
}
