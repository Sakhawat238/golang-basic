package main

import (
	"fmt"
)

func main() {
	x := 5
	y := 5
	z := 6

	s1 := "gg"
	s2 := "GG"

	val1 := x < 5
	val2 := x <= 5
	val3 := x >= y
	val4 := y != z
	val5 := y == z

	val6 := s1 == s2
	val7 := s1 > s2
	val8 := s1 < s2

	val9 := x <= 5 && x >= y
	val10 := x < 5 || x <= 5
	val11 := (s1 == s2) == (s1 > s2)
	val12 := (s1 == s2) != (s1 > s2)

	val13 := true || false && true
	val14 := false && true || false
	val15 := false && true || true
	val16 := false && !false || true

	fmt.Println(val1, val2, val3, val4, val5)
	fmt.Println(val6, val7, val8)
	fmt.Println(val9, val10, val11, val12)
	fmt.Println(val13, val14, val15, val16)
}
