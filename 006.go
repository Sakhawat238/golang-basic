package main

import (
	"fmt"
)

func main() {
	a := true
	b := false

	if a {
		fmt.Println("True")
	}

	if b {
		fmt.Println("True")
	} else if a == b {
		fmt.Println("True")
	} else {
		fmt.Println("False")
	}
}
