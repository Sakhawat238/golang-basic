package main

import "fmt"

func main() {
	fmt.Printf("%T \n", 10)
	fmt.Printf("%v \n", 10)
	fmt.Printf("%% \n")
	fmt.Printf("%t \n", true)
	fmt.Printf("%b \n", 10)
	fmt.Printf("%o \n", 10)
	fmt.Printf("%d \n", 10)
	fmt.Printf("%x \n", 10)
	fmt.Printf("%e \n", 10.50)
	fmt.Printf("%f \n", 10.50)
	fmt.Printf("%f \n", 101210.5042343241112)
	fmt.Printf("%4f \n", 101210.5042343241112)
	fmt.Printf("%4.5f \n", 101210.5042343241112)
	fmt.Printf("%4.f \n", 101210.5042343241112)
	fmt.Printf("%.5f \n", 101210.5042343241112)
	fmt.Printf("%F \n", 10.50)
	fmt.Printf("%g \n", 10.5432423413131)
	fmt.Printf("%s \n", "hello")
	fmt.Printf("%q \n", "hello")
}
